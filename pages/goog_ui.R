library(googleVis)
pageGoog <- function(){
  tagList(
    h1("Google Vis"),
    htmlOutput("googChart")
  )
}