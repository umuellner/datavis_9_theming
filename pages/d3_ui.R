source("util/d3_linechart.R")
pageD3 <- function(){
  tagList(
    h1("D3"),
    d3LineChartOutput("d3Chart")
  )
}